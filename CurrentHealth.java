
package org.emids.insurance.health;

public class CurrentHealth {
	private boolean isThereHypertension;
	private boolean isThereBloodPressure;
	private boolean isThereBloodSugar;
	private boolean isThereOverWeigth;

	public CurrentHealth(boolean isThereHypertension, boolean isThereBloodPressure, boolean isThereBloodSugar,
			boolean isThereOverWeigth) {
		super();
		this.isThereHypertension = isThereHypertension;
		this.isThereBloodPressure = isThereBloodPressure;
		this.isThereBloodSugar = isThereBloodSugar;
		this.isThereOverWeigth = isThereOverWeigth;
	}

	public CurrentHealth() {
	}

	public boolean isThereHypertension() {
		return isThereHypertension;
	}

	public void setThereHypertension(boolean isThereHypertension) {
		this.isThereHypertension = isThereHypertension;
	}

	public boolean isThereBloodPressure() {
		return isThereBloodPressure;
	}

	public void setThereBloodPressure(boolean isThereBloodPressure) {
		this.isThereBloodPressure = isThereBloodPressure;
	}

	public boolean isThereBloodSugar() {
		return isThereBloodSugar;
	}

	public void setThereBloodSugar(boolean isThereBloodSugar) {
		this.isThereBloodSugar = isThereBloodSugar;
	}

	public boolean isThereOverWeigth() {
		return isThereOverWeigth;
	}

	public void setThereOverWeigth(boolean isThereOverWeigth) {
		this.isThereOverWeigth = isThereOverWeigth;
	}

	@Override
	public String toString() {
		return "CurrentHealth [isThereHypertension=" + isThereHypertension + ", isThereBloodPressure="
				+ isThereBloodPressure + ", isThereBloodSugar=" + isThereBloodSugar + ", isThereOverWeigth="
				+ isThereOverWeigth + "]";
	}
}
