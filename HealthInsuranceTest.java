
package org.emids.insurance.health;

public class HealthInsuranceTest {
	public static void main(String[] args) {
		HealthInsurance hi = new HealthInsurance();
		Person p = new Person("Norman Gomes", 34, HealthInsurance.gender_M);
		CurrentHealth ch = new CurrentHealth(false, false, false, true);
		Habits h = new Habits(false, true, true, false);
		p.setCurrentHealth(ch);
		p.setHabits(h);
		System.out.println("Health Insurance Premium for Mr." + p.getName() + " Rs." + hi.getPermium(p));
	}
}
