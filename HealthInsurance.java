
package org.emids.insurance.health;

public class HealthInsurance {
	public double premium_Basic = 5000;
	static String gender_M = new String("Male");
	static String gender_F = new String("Female");
	public double premium = 0;

	double getPermium(Person p) {
		calculatePermiumBasedOnAge(p.getAge());
		calculatePermiumBasedOnGender(p.getGender());
		calculatePermiumBasedOnCurrentHealth(p.getCurrentHealth());
		calculatePermiumBasedOnHabits(p.getHabits());
		return premium;
	}

	public double calculatePermiumBasedOnAge(int age) {
		if (age < 18) {
			premium += premium_Basic;
		} else {
			premium += premium_Basic;
			if (age > 18 && age <= 25) {
				premium = increaseBy10Per(premium);
			} else if (age > 25 && age <= 30) {
				premium = increaseBy10Per(increaseBy10Per(premium));
			} else if (age > 30 && age <= 35) {
				premium = increaseBy10Per(increaseBy10Per(increaseBy10Per(premium)));
			} else if (age > 35 && age <= 40) {
				premium = increaseBy10Per(increaseBy10Per(increaseBy10Per(increaseBy10Per(premium))));
			} else if (age > 40 && age <= 100) {
				int inc_age = age - 40;
				while (inc_age > 0) {
					premium = increaseBy10Per(increaseBy10Per(premium));
					inc_age = inc_age - 5;
				}
			}
		}
		return premium;
	}

	public double calculatePermiumBasedOnGender(String gender) {
		if (gender_M.equalsIgnoreCase(gender)) {
			premium = increaseBy2Per(premium);
		} else if (gender_F.equalsIgnoreCase(gender)) {
			premium = increaseBy2Per(increaseBy2Per(premium));
		} else {
			premium = increaseBy2Per(increaseBy2Per(increaseBy2Per(premium)));
		}
		return premium;
	}

	public double calculatePermiumBasedOnCurrentHealth(CurrentHealth ch) {
		if (ch.isThereBloodPressure()) {
			premium = increaseBy1Per(premium);
		}
		if (ch.isThereBloodSugar()) {
			premium = increaseBy1Per(premium);
		}
		if (ch.isThereHypertension()) {
			premium = increaseBy1Per(premium);
		}
		if (ch.isThereOverWeigth()) {
			premium = increaseBy1Per(premium);
		}
		return premium;
	}

	public double calculatePermiumBasedOnHabits(Habits h) {
		if (h.isDailyExercise()) {
			premium = decreaseBy3Per(premium);
		}
		if (h.isAlcohol()) {
			premium = increaseBy3Per(premium);
		}
		if (h.isDrugs()) {
			premium = increaseBy3Per(premium);
		}
		if (h.isSmoking()) {
			premium = increaseBy3Per(premium);
		}
		return premium;
	}

	public double increaseBy1Per(double p) {
		p += p * 0.01;
		return p;
	}

	public double increaseBy2Per(double p) {
		p += p * 0.02;
		return p;
	}

	public double increaseBy3Per(double p) {
		p += p * 0.03;
		return p;
	}

	double decreaseBy3Per(double p) {
		p -= p * 0.03;
		return p;
	}

	public double increaseBy10Per(double p) {
		p += p * 0.10;
		return p;
	}
}
