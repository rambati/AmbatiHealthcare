
package org.emids.insurance.health;

public class Habits {
	private boolean smoking;
	private boolean alcohol;
	private boolean dailyExercise;
	private boolean drugs;

	public Habits(boolean smoking, boolean alcohol, boolean dailyExercise, boolean drugs) {
		super();
		this.smoking = smoking;
		this.alcohol = alcohol;
		this.dailyExercise = dailyExercise;
		this.drugs = drugs;
	}

	public Habits() {
	}

	public boolean isSmoking() {
		return smoking;
	}

	public void setSmoking(boolean smoking) {
		this.smoking = smoking;
	}

	public boolean isAlcohol() {
		return alcohol;
	}

	public void setAlcohol(boolean alcohol) {
		this.alcohol = alcohol;
	}

	public boolean isDailyExercise() {
		return dailyExercise;
	}

	public void setDailyExercise(boolean dailyExercise) {
		this.dailyExercise = dailyExercise;
	}

	public boolean isDrugs() {
		return drugs;
	}

	public void setDrugs(boolean drugs) {
		this.drugs = drugs;
	}

	@Override
	public String toString() {
		return "Habits [smoking=" + smoking + ", alcohol=" + alcohol + ", dailyExercise=" + dailyExercise + ", drugs="
				+ drugs + "]";
	}
}
