
package org.emids.insurance.health;

public class Person {
	private String name;
	private int age;
	private String gender;
	private CurrentHealth currentHealth;
	private Habits habits;

	public Person(String name, int age, String gender) {
		super();
		this.name = name;
		this.age = age;
		this.gender = gender;
	}

	public Person() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public CurrentHealth getCurrentHealth() {
		return currentHealth;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", age=" + age + ", gender=" + gender + ", currentHealth=" + currentHealth
				+ ", habits=" + habits + "]";
	}

	public void setCurrentHealth(CurrentHealth currentHealth) {
		this.currentHealth = currentHealth;
	}

	public Habits getHabits() {
		return habits;
	}

	public void setHabits(Habits habits) {
		this.habits = habits;
	}
}
